package com.htc.spbootjdbcdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpbootjdbcdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpbootjdbcdemoApplication.class, args);
	}

}
