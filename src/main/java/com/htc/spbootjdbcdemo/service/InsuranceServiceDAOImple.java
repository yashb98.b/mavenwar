package com.htc.spbootjdbcdemo.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.htc.spbootjdbcdemo.model.Customer;
import com.htc.spbootjdbcdemo.model.Policy;
import com.htc.spbootjdbcdemo.to.CustomerPolicyTO;
import com.htc.spbootjdbcdemo.to.CustomerTO;
import com.htc.spbootjdbcdemo.to.PolicyTO;


@Service
public class InsuranceServiceDAOImple implements InsuranceServiceDAO
{
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	private CustomerTO getcustomerTO(Customer customer) 
	{
		CustomerTO customerTO = new CustomerTO();
		customerTO.setCustomerCode(customer.getCustomerCode());
		customerTO.setCustomerName(customer.getCustomerName());
		customerTO.setAddress(customer.getAddress());
		customerTO.setContactno(customer.getContactno());
		return customerTO;
	}
	
	private Customer getCustomer(CustomerTO customerTO) 
	{
		Customer customer = new Customer();
		customer.setCustomerCode(customerTO.getCustomerCode());
		customer.setCustomerName(customerTO.getCustomerName());
		customer.setAddress(customerTO.getAddress());
		customer.setContactno(customerTO.getContactno());
		return customer;
	}
	
	private Policy getPolicy(PolicyTO policyTO) {
		Policy policy = new Policy();
		policy.setPolicyno(policyTO.getPolicyno());
		policy.setPremium(policyTO.getPremium());
		policy.setStartdate(policyTO.getStartdate());
		policy.setPolicymode(policyTO.getPolicymode());
		
		return policy;
	}
	@Override
	public boolean addCustomer(CustomerTO customer) {
		boolean addStatus = false;
		int result = jdbcTemplate.update("insert into customer values(?,?,?,?)", customer.getCustomerCode(),customer.getCustomerName(),customer.getAddress(),customer.getContactno());
		if(result == 1 )
			addStatus =true;
		
		return addStatus;
	}

	@Override
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		boolean addStatus=false;
		int result= jdbcTemplate.update("insert into customer values(?,?,?,?)", customerTO.getCustomerCode(),customerTO.getCustomerName(),customerTO.getAddress(),customerTO.getContactno());
		int status=jdbcTemplate.update("insert into policy values(?,?,?,?,?)",policyTO.getPolicyno(),policyTO.getPremium(),policyTO.getStartdate(),policyTO.getPolicymode(),policyTO.getCustomerTO());
		if(result==1 && status == 1)
			addStatus = true;
		return addStatus;
	}

	@Override
	public boolean takeNewPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		System.out.println("in dao take new policy");
		Customer customer = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);
		boolean addStatus = false;
		System.out.println(customer);
		System.out.println(policy);
		int noOfRowsAffected = jdbcTemplate.update("insert into policy values(?,?,?,?,?)",policy.getPolicyno(),customer.getCustomerCode(),policy.getPremium(),policy.getStartdate(),policy.getPolicymode());
		if(noOfRowsAffected==1) {
			System.out.println("added using jdbc");
			addStatus=true;
		}
		return addStatus;
		/*if(customerTO==null) 
		{
			System.out.println(customerTO);
			return false;
		}
		int result=jdbcTemplate.update("insert into policy values(?,?,?,?,?)", policyTO.getPolicyno(),policyTO.getCustomerTO(),policyTO.getPremium(),policyTO.getStartdate(),policyTO.getPolicymode());
		//System.out.println(result);
		if(result==1)
			System.out.println(result);
			addStatus=true;
		
		return addStatus;*/
	}

	@Override
	public List<Policy> getInsurancePolicies(String customerCode) {
		List<Policy> policies=null;
		policies= jdbcTemplate.query("select policyno,premium,startdate,policymode from policy where customerCode=?",
				new RowMapper<Policy>() 
		{

			
			public Policy mapRow(ResultSet rs, int rownum) throws SQLException {
				System.out.println(rownum);
				Policy p = new Policy();
				p.setPolicyno(rs.getLong(1));
				p.setPremium(rs.getDouble(2));
				p.setStartdate(rs.getDate(3));
				p.setPolicymode(rs.getString(4));
				return p;
			}
			
		},  
		customerCode);
return policies;

	}

	@Override
	public PolicyTO getInsurancePolicy(long policyno) {
		PolicyTO policyTO = null;
		try {
		policyTO = jdbcTemplate.queryForObject("select policyno,premium,startdate,policymode from policy where policyno=?", 
				new RowMapper<PolicyTO>() 
				{
					public PolicyTO mapRow(ResultSet rs, int rownum) throws SQLException {
						System.out.println(rownum);
						PolicyTO p = new PolicyTO();
						p.setPolicyno(rs.getLong(1));
						p.setPremium(rs.getDouble(2));
						p.setStartdate(rs.getDate(3));
						p.setPolicymode(rs.getString(4));
						return p;
					}
					
				},  
				policyno);
		}catch(Exception ex) 
		{
			return null;
		}
		return policyTO;
	}

	

}
