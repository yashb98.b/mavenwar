package com.htc.spbootjdbcdemo.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.spbootjdbcdemo.model.Customer;
import com.htc.spbootjdbcdemo.model.Policy;
import com.htc.spbootjdbcdemo.repository.CustomerRepository;
import com.htc.spbootjdbcdemo.repository.PolicyRepository;
import com.htc.spbootjdbcdemo.to.CustomerTO;
import com.htc.spbootjdbcdemo.to.PolicyTO;

@Service
public class InsuranceServiceImple implements InsuranceService
{
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	PolicyRepository policyRepository;
	
	private CustomerTO getcustomerTO(Customer customer) 
	{
		CustomerTO customerTO = new CustomerTO();
		customerTO.setCustomerCode(customer.getCustomerCode());
		customerTO.setCustomerName(customer.getCustomerName());
		customerTO.setAddress(customer.getAddress());
		customerTO.setContactno(customer.getContactno());
		return customerTO;
	}
	
	private Customer getCustomer(CustomerTO customerTO) 
	{
		Customer customer = new Customer();
		customer.setCustomerCode(customerTO.getCustomerCode());
		customer.setCustomerName(customerTO.getCustomerName());
		customer.setAddress(customerTO.getAddress());
		customer.setContactno(customerTO.getContactno());
		return customer;
	}
	
	private Policy getPolicy(PolicyTO policyTO) {
		Policy policy = new Policy();
		policy.setPolicyno(policyTO.getPolicyno());
		policy.setPremium(policyTO.getPremium());
		policy.setStartdate(policyTO.getStartdate());
		policy.setPolicymode(policyTO.getPolicymode());
		
		return policy;
	}
	
	private PolicyTO getPolicyTO(Policy policy) {
		PolicyTO policyTO =new PolicyTO();
		policyTO.setPolicyno(policy.getPolicyno());
		policyTO.setPremium(policy.getPremium());
		policyTO.setStartdate(policy.getStartdate());
		policyTO.setPolicymode(policy.getPolicymode());
		
		return policyTO;
	}
	@Override
	public boolean addCustomer(CustomerTO customerTO) {
		if(customerRepository.save(getCustomer(customerTO))!=null)
			return true;
		else
			return false;
	}


	@Override
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		Customer customer = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);
		
		System.out.println(customer);
		System.out.println(policy);
		
		Set<Policy> policies= customer.getPolicies();
		if(policies ==null) {
			policies= new HashSet<Policy>();
		}
		policies.add(policy);
		customer.setPolicies(policies);
		policy.setCustomer(customer);
		
		customerRepository.save(customer);
		return true;
	}

	@Override
	public PolicyTO getInsurancePolicy(long policyno) {
		Optional<Policy> optPolicy = policyRepository.findById(policyno);
		if(optPolicy.isPresent()) {
			return getPolicyTO(optPolicy.get());
		}
		return null;
	}

	@Override
	public Set<Policy> getInsurancePolicies(String customerCode) {
		//Optional<Policy> optPolicy = policyRepository.findById(customerCode);
		Optional<Customer> optCustomer = customerRepository.findById(customerCode);
		if(optCustomer.isPresent()) 
		{
			Customer customer = optCustomer.get();
			System.out.println(customer);
			return customer.getPolicies();
		}
		return null;
	}

	

	@Override
	public Customer getCustomerDetails(long policyno) {
		Optional<Policy> optionalPolicy = policyRepository.findById(policyno);
		if(optionalPolicy.isPresent()) 
		{
			Policy policy=optionalPolicy.get();
			System.out.println(optionalPolicy);
			return policy.getCustomer();
			
		}
		return null;
	}

	@Override
	public boolean takeNewPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		Customer customer = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);
		
		System.out.println(customer);
		System.out.println(policy);
		policy.setCustomer(customer);
		policyRepository.save(policy);
		return true;
	}


}
