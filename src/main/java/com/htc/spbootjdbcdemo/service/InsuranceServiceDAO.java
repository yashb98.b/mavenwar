package com.htc.spbootjdbcdemo.service;

import java.util.List;
import java.util.Set;

import com.htc.spbootjdbcdemo.model.Customer;
import com.htc.spbootjdbcdemo.model.Policy;
import com.htc.spbootjdbcdemo.to.CustomerPolicyTO;
import com.htc.spbootjdbcdemo.to.CustomerTO;
import com.htc.spbootjdbcdemo.to.PolicyTO;

public interface InsuranceServiceDAO 
{
	public boolean addCustomer(CustomerTO customer);
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO);
	public boolean takeNewPolicy(CustomerTO customerTO,PolicyTO policyTO);
	public List<Policy> getInsurancePolicies(String customerCode);
	public PolicyTO getInsurancePolicy(long policyno);
	//public CustomerPolicyTO getCustomerDetails(long policyno);

}
