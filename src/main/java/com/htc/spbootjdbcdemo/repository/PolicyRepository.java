package com.htc.spbootjdbcdemo.repository;

import org.springframework.data.repository.CrudRepository;

import com.htc.spbootjdbcdemo.model.Policy;

public interface PolicyRepository extends CrudRepository<Policy, Long>
{

}
