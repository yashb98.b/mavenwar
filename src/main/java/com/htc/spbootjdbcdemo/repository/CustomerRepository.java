package com.htc.spbootjdbcdemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.htc.spbootjdbcdemo.model.Customer;
import com.htc.spbootjdbcdemo.to.CustomerTO;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String>
{
	
}
