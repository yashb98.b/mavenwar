package com.htc.spbootjdbcdemo.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.htc.spbootjdbcdemo.model.Customer;
import com.htc.spbootjdbcdemo.model.Policy;
import com.htc.spbootjdbcdemo.service.InsuranceServiceDAO;
import com.htc.spbootjdbcdemo.to.CustomerPolicyTO;
import com.htc.spbootjdbcdemo.to.CustomerTO;
import com.htc.spbootjdbcdemo.to.PolicyTO;

@Controller
public class InsurancePolicyDAOController 
{
	@Autowired
	InsuranceServiceDAO insuranceServiceDAO;
	
	@RequestMapping(value="/addcustomerform", method=RequestMethod.GET)
	public String showCustomerForm() {
		return "addcustomerform";
	}
	@PostMapping("/saveCustomer")    //spring 4.x onwards
	public String saveCustomer(@ModelAttribute(name="customer") @Valid CustomerTO customerTO, BindingResult bresult, RedirectAttributes redirectAttributes) {
		if(bresult.hasErrors())
			return "customerform";
		System.out.println(customerTO);
		boolean result = insuranceServiceDAO.addCustomer(customerTO);
		if(result) 
		{
			redirectAttributes.addFlashAttribute("customerName", customerTO.getCustomerName());
			redirectAttributes.addFlashAttribute("msg", "Added successfully");
			return "redirect:/addCustomerSuccess";
		}
		else
			return "customerform";
	}
	@GetMapping("/findPolicyForm")
	public String findPolicyForm() {
		return "findpolicyform";
	}
	@PostMapping("/getPolicyDetails")
	public ModelAndView getPolicyDetails(@RequestParam(name="policyno") long policyno) {
		
		ModelAndView mv = new ModelAndView();
		PolicyTO policyTO = insuranceServiceDAO.getInsurancePolicy(policyno);
		System.out.println(policyTO);
		if(policyTO==null) {
			mv.setViewName("policydetailsmissing");		
			mv.addObject("policyno", policyno);
		}
		else {
			mv.setViewName("policydetails");
			mv.addObject("policyTO", policyTO);
		}
		return mv;
	}
	@GetMapping("/findInsurancePolicies")
	public String findInsurancePolicies() {
		return "findinsurancepolicies";
	}
	@PostMapping("/retrievePolicies")
	public ModelAndView getInsurancePolicies(@RequestParam(name="customerCode") String customerCode) 
	{
		ModelAndView mv = new ModelAndView();
		List<Policy> policies = insuranceServiceDAO.getInsurancePolicies(customerCode); 
		
		if(policies.isEmpty()) 
		{
			mv.setViewName("policydetailsmissing");
			mv.addObject("customerCode", customerCode);
		}
		else 
		{
			mv.setViewName("policyinfo");
			mv.addObject("policies",policies);
			System.out.println(policies);
		}
		return mv;
	}
	/*@GetMapping("/findCustomerPolicies")
	public String findCustomerPolicies() {
		return "findcustomerpolicies";
	}*/
	
	//@RequestMapping(value="/check", method=RequestMethod.POST)
	/*@PostMapping("/getCustomerDetails")
	public ModelAndView getCustomerDetails(@RequestParam(name="policyno") long policyno) 
	{
		ModelAndView mv = new ModelAndView();
		CustomerPolicyTO customerpolicyTO = insuranceServiceDAO.getCustomerDetails(policyno);
		System.out.println(customerpolicyTO);
		if(customerpolicyTO == null) 
		{
			mv.setViewName("customerdetailsmissing");
			mv.addObject("policyno", policyno);
		}
		else 
		{
			mv.setViewName("customerdetails");
			mv.addObject("Customer", customerpolicyTO);
		}
		return mv;
	}*/
	@GetMapping("/addNewPolicy")
	public String addnewpolicy() {
		return "addnewpolicy";
	}
	@PostMapping("/givePolicyToCustomer")
	public String takeNewPolicy(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		//String customerCode=customerPolicyTO.getCustomerCode();
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactno());
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyno(), customerPolicyTO.getPremium(), customerPolicyTO.getStartdate(), customerPolicyTO.getPolicymode());
		if(insuranceServiceDAO.takeNewPolicy(customerTO, policyTO)) 
		{
			
			System.out.println("success");
			attributes.addFlashAttribute("policyNo", customerPolicyTO.getPolicyno());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else {
			System.out.println("take-newpolicy");
			
		}
		return null;		
	}
	@RequestMapping(value="/givePolicyForm", method=RequestMethod.GET)
	public String givePolicyToCustomerForm() {
		return "policyform2";
	}
	@RequestMapping(value="/givePolicy", method=RequestMethod.POST)
	public String issuePolicyToCustomer(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		
		System.out.println(customerPolicyTO);
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactno());;
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyno(), customerPolicyTO.getPremium(), customerPolicyTO.getStartdate(), customerPolicyTO.getPolicymode());;
		
		System.out.println(customerTO);
		System.out.println(policyTO);
		
		boolean result = insuranceServiceDAO.addCustomerWithPolicy(customerTO, policyTO);
		
		if(result) {
			attributes.addFlashAttribute("policyno", customerPolicyTO.getPolicyno());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess2";
		}
		else 
			return "policyform";
	}
	@RequestMapping(value="/issuePolicySuccess2", method=RequestMethod.GET)
	public String issuePolicySuccess() {
		return "issuepolicysuccess2";
	}
	/*@GetMapping(value="/getPolicyDetailsAJAX", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PolicyTO getPolicyDetailsAJAX(@RequestParam(name="policyno") long policyno) 
	{
		ModelAndView mv= new ModelAndView();
		PolicyTO policyTO=insuranceServiceDAO.getInsurancePolicy(policyno);
		System.out.println(policyTO);
		if(policyTO==null) {
			return null;
		}
		else {
			return policyTO;
		}
	}*/
}
