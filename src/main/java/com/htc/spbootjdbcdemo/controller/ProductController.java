package com.htc.spbootjdbcdemo.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.htc.spbootjdbcdemo.model.Product;

@Controller
public class ProductController 
{
	@GetMapping("/productform")
	public String productForm() {
		return "productform";
	}
	@PostMapping("/addProduct")
	public String addProduct(Product product) 
	{
		RestTemplate restTemplate=new RestTemplate();
		
		boolean result = restTemplate.postForObject("http://localhost:8080/api/products", product, Boolean.class);
		if(result)
			return "addproductsuccess";
		else
			return "addproductfailure";
	}
	@GetMapping("/searchProduct")
	public String searchProduct() {
		return "searchproduct";
	}
	@PostMapping("/searchProduct")
	public ModelAndView getProduct(@RequestParam(name="productCode") String productCode) {
		System.out.println(productCode);
		RestTemplate restTemplate=new RestTemplate();
		Product p=restTemplate.getForObject("http://localhost:8080/api/products/" + productCode, Product.class);
		//System.out.println(p);
		ModelAndView mv=new ModelAndView("showproduct","product",p);
		System.out.println(mv);
		return mv;
	}
	@GetMapping("/listProduct")
	public String listProduct() {
		return "listproduct";
	}
	
	
		
	
}
