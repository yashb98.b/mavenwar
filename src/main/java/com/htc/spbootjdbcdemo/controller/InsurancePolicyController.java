package com.htc.spbootjdbcdemo.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.htc.spbootjdbcdemo.model.Customer;
import com.htc.spbootjdbcdemo.model.Policy;
import com.htc.spbootjdbcdemo.service.InsuranceService;
import com.htc.spbootjdbcdemo.to.CustomerPolicyTO;
import com.htc.spbootjdbcdemo.to.CustomerResponse;
import com.htc.spbootjdbcdemo.to.CustomerTO;
import com.htc.spbootjdbcdemo.to.PolicyTO;

@Controller
public class InsurancePolicyController 
{
	@Autowired
	InsuranceService insuranceService;
	
	
	@RequestMapping(value="/customerform", method=RequestMethod.GET)
	public String showCustomerForm() {
		return "customerform";
	}
	
	@PostMapping("/addCustomer")    //spring 4.x onwards
	public String addCustomer(@ModelAttribute(name="customer") @Valid CustomerTO customerTO, BindingResult bresult, RedirectAttributes redirectAttributes) {
		if(bresult.hasErrors())
			return "customerform";
		System.out.println(customerTO);
		boolean result = insuranceService.addCustomer(customerTO);
		if(result) 
		{
			redirectAttributes.addFlashAttribute("customerName", customerTO.getCustomerName());
			redirectAttributes.addFlashAttribute("msg", "Added successfully");
			return "redirect:/addCustomerSuccess";
		}
		else
			return "customerform";
	
	}
	
	@GetMapping("/addCustomerSuccess")
	public String addCustomerSuccess() {
		return "customeraddsuccess";
	}
	
	@RequestMapping(value="/issuePolicyForm", method=RequestMethod.GET)
	public String issuePolicyToCustomerForm() {
		return "policyform";
	}
	@RequestMapping(value="/issuePolicy", method=RequestMethod.POST)
	public String issuePolicyToCustomer(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		
		System.out.println(customerPolicyTO);
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactno());;
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyno(), customerPolicyTO.getPremium(), customerPolicyTO.getStartdate(), customerPolicyTO.getPolicymode());;
		
		System.out.println(customerTO);
		System.out.println(policyTO);
		
		boolean result = insuranceService.addCustomerWithPolicy(customerTO, policyTO);
		
		if(result) {
			attributes.addFlashAttribute("policyno", customerPolicyTO.getPolicyno());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else 
			return "policyform";
	}
	@RequestMapping(value="/issuePolicySuccess", method=RequestMethod.GET)
	public String issuePolicySuccess() {
		return "issuepolicysuccess";
	}
	@GetMapping("/searchPolicyForm")
	public String searchPolicyForm() {
		return "searchpolicyform";
	}
	@PostMapping("/getPolicyInfo")
	public ModelAndView getPolicyDetails(@RequestParam(name="policyno") long policyno) {
		
		ModelAndView mv = new ModelAndView();
		PolicyTO policyTO = insuranceService.getInsurancePolicy(policyno);
		if(policyTO == null) {
			mv.setViewName("policydetailsmissing");		
			mv.addObject("policyno", policyno);
		}
		else {
			mv.setViewName("policydetails");
			mv.addObject("policyTO", policyTO);
		}
		return mv;
	}
	
	@GetMapping("/searchCustomerPolicies")
	public String searchCustomerPolicies() {
		return "searchcustomerpolicies";
	}
	
	//@RequestMapping(value="/check", method=RequestMethod.POST)
	@PostMapping("/getCustomerInfo")
	public ModelAndView getCustomerDetails(@RequestParam(name="policyno",required = false, defaultValue="0") long policyno) 
	{
		ModelAndView mv = new ModelAndView();
		Customer customer = insuranceService.getCustomerDetails(policyno);
		System.out.println(customer);
		if(customer == null) 
		{
			mv.setViewName("customerdetailsmissing");
			mv.addObject("policyno", policyno);
		}
		else 
		{
			mv.setViewName("customerdetails");
			mv.addObject("Customer", customer);
		}
		return mv;
	}
	@GetMapping("/searchInsurancePolicies")
	public String searchInsurancePolicies() {
		return "searchinsurancepolicies";
	}
	@PostMapping("/getPolicies")
	public ModelAndView getInsurancePolicies(@RequestParam(name="customerCode") String customerCode) 
	{
		ModelAndView mv = new ModelAndView();
		Set<Policy> policies = insuranceService.getInsurancePolicies(customerCode); 
		if(policies == null) 
		{
			mv.setViewName("policydetailsmissing");
			mv.addObject("customerCode", customerCode);
		}
		else 
		{
			mv.setViewName("policyinfo");
			mv.addObject("policies",policies);
			System.out.println(policies);
		}
		return mv;
	}
	
	@GetMapping("/takeNewPolicy")
	public String takenewpolicy() {
		return "takenewpolicy";
	}
	@PostMapping("/addPolicyToCustomer")
	public String takeNewPolicy(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		//String customerCode=customerPolicyTO.getCustomerCode();
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactno());
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyno(), customerPolicyTO.getPremium(), customerPolicyTO.getStartdate(), customerPolicyTO.getPolicymode());
		if(insuranceService.takeNewPolicy(customerTO, policyTO)) {
			System.out.println("success");
			attributes.addFlashAttribute("policyNo", customerPolicyTO.getPolicyno());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else {
			System.out.println("take-newpolicy");
			
		}
		return null;		
	}
	@ExceptionHandler
	public ModelAndView execeptionHandler(Exception ex) {
		ModelAndView mv = new ModelAndView("error", "errorMsg", ex.toString());
		return mv;
	}

}
