package com.htc.spbootjdbcdemo.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExceptionHandler 
{
	@ExceptionHandler
	public ModelAndView handleGlobalExceptions(Exception ex) {
		ModelAndView mv = new ModelAndView("error", "errorMsg", ex.toString());
		return mv;
	}
}
