package com.htc.spbootjdbcdemo.to;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.htc.spbootjdbcdemo.model.Policy;


public class CustomerTO implements Serializable
{
	@NotBlank(message="customercode is mandatory")
	private String customerCode;
	@NotBlank(message="customerName is mandatory")
	@Size(min=5, max=15, message="Name should be between 5 and 15")
	private String customerName;
	@NotBlank(message="address is mandatory")
	@Size(min=7,max=50, message="Address should be between 7 and 50")
	private String address;
	@NotBlank(message="contactno is mandatory")
	@Size(min=10,max=10,message="contactno should be 10 digits")
	private String contactno;
	
	@OneToMany(mappedBy="customerTO",cascade= CascadeType.ALL)
	Set<PolicyTO> policies;
	public CustomerTO() {}

	public CustomerTO(String customerCode, String customerName, String address, String contactno) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactno = contactno;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}


	public Set<PolicyTO> getPolicies() {
		return policies;
	}

	public void setPolicies(Set<PolicyTO> policies) {
		this.policies = policies;
	}

	@Override
	public String toString() {
		return "CustomerTO [customerCode=" + customerCode + ", customerName=" + customerName + ", address=" + address
				+ ", contactno=" + contactno + "]";
	}
}
