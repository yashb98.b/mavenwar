package com.htc.spbootjdbcdemo.to;

import java.util.Date;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

import com.htc.spbootjdbcdemo.model.Customer;
//@XmlRootElement
public class PolicyTO {
	
	private long policyno;
	private double premium;
	private Date startdate;
	private String policymode;
	
	@ManyToOne
	@JoinColumn(name="CUSTOMERCODE" )
	CustomerTO customerTO;
	
	public PolicyTO() {}
	public PolicyTO(long policyno, double premium,  Date startdate, String policymode) {
		super();
		this.policyno = policyno;
		this.premium = premium;
		this.startdate = startdate;
		this.policymode = policymode;
	}
	public long getPolicyno() {
		return policyno;
	}
	public void setPolicyno(long policyno) {
		this.policyno = policyno;
	}
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public String getPolicymode() {
		return policymode;
	}
	public void setPolicymode(String policymode) {
		this.policymode = policymode;
	}
	
	
	public CustomerTO getCustomerTO() {
		return customerTO;
	}
	public void setCustomerTO(CustomerTO customerTO) {
		this.customerTO = customerTO;
	}
	@Override
	public String toString() {
		return "PolicyTO [policyno=" + policyno + ", premium=" + premium + ", startdate=" + startdate + ", policymode="
				+ policymode + "]";
	}
	
	
}
