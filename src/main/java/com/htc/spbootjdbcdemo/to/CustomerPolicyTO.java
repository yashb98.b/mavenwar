package com.htc.spbootjdbcdemo.to;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class CustomerPolicyTO {
	@NotBlank(message="customercode is mandatory")
	private String customerCode;
	@NotBlank(message="customerName is mandatory")
	@Size(min=5, max=15, message="Name should be between 5 and 15")
	private String customerName;
	@NotBlank(message="address is mandatory")
	@Size(min=7,max=50, message="Address should be between 7 and 50")
	private String address;
	@NotBlank(message="contactno is mandatory")
	@Size(min=10,max=10,message="contactno should be 10 digits")
	private String contactno;
	
	private long policyno;
	private double premium;
	@DateTimeFormat(pattern="dd-MM-yyyy")
	private Date startdate;
	private String policymode;
	
	
	public CustomerPolicyTO() {
		
	}

	public CustomerPolicyTO(@NotBlank(message = "customercode is mandatory") String customerCode,
			@NotBlank(message = "customerName is mandatory") @Size(min = 5, max = 15, message = "Name should be between 5 and 15") String customerName,
			@NotBlank(message = "address is mandatory") @Size(min = 7, max = 50, message = "Address should be between 7 and 50") String address,
			@NotBlank(message = "contactno is mandatory") @Size(min = 10, max = 10, message = "contactno should be 10 digits") String contactno,
			long policyno, double premium, Date startdate, String policymode) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactno = contactno;
		this.policyno = policyno;
		this.premium = premium;
		this.startdate = startdate;
		this.policymode = policymode;
		
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public long getPolicyno() {
		return policyno;
	}

	public void setPolicyno(long policyno) {
		this.policyno = policyno;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}
	
	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}


	public String getPolicymode() {
		return policymode;
	}

	public void setPolicymode(String policymode) {
		this.policymode = policymode;
	}

	@Override
	public String toString() {
		return "CustomerPolicyTO [customerCode=" + customerCode + ", customerName=" + customerName + ", address="
				+ address + ", contactno=" + contactno + ", policyno=" + policyno + ", premium=" + premium
				+ ", startdate=" + startdate + ", policymode=" + policymode + "]";
	}

	
	

}
