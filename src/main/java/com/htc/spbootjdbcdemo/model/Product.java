package com.htc.spbootjdbcdemo.model;

public class Product 
{
	private String productCode;
	private String productName;
	private String category;
	private int price; 
	private String quantity;
	
	public Product() {}
	
	public Product(String productCode, String productName, String category, int price, String quantity) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.category = category;
		this.price = price;
		this.quantity = quantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Product [productCode=" + productCode + ", productName=" + productName + ", category=" + category
				+ ", price=" + price + ", quantity=" + quantity + "]";
	}
	

}

