package com.htc.spbootjdbcdemo.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name="CUSTOMER")
public class Customer 
{
	@Id
	@Column(name="CUSTOMERCODE")
	//@NotBlank(message="customercode is mandatory")
	private String customerCode;
	@Column(name="CUSTOMERNAME")
	//@NotBlank(message="customerName is mandatory")
	//@Size(min=5, max=15, message="Name should be between 5 and 15")
	private String customerName;
	//@NotBlank(message="address is mandatory")
	//@Size(min=7,max=50, message="Address should be between 7 and 50")
	private String address;
	//@NotBlank(message="contactno is mandatory")
	//@Size(min=10,max=10,message="contactno should be 10 digits")
	private String contactno;
	
	@OneToMany(mappedBy="customer",cascade= CascadeType.ALL,fetch = FetchType.EAGER)
	Set<Policy> policies;

	public Customer() {}

	public Customer(String customerCode, String customerName, String address, String contactno) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactno = contactno;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	

	public Set<Policy> getPolicies() {
		return policies;
	}

	public void setPolicies(Set<Policy> policies) {
		this.policies = policies;
	}

	@Override
	public String toString() {
		return "Customer [customerCode=" + customerCode + ", customerName=" + customerName + ", address=" + address
				+ ", contactno=" + contactno + "]";
	}
	
	
}

