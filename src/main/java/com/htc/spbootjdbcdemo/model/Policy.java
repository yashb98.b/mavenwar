package com.htc.spbootjdbcdemo.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@Table(name="policy")
public class Policy 
{
	@Id
	private long policyno;
	@Column
	private double premium;
	@Column
	@Temporal(TemporalType.DATE)
	private Date startdate;
	private String policymode;
	
	@ManyToOne
	@JoinColumn(name="CUSTOMERCODE" )
	Customer customer;
		
	public Policy() {}

	public Policy(long policyno, double premium, Date startdate, String policymode) {
		super();
		this.policyno = policyno;
		this.premium = premium;
		this.startdate = startdate;
		this.policymode = policymode;
	}

	public long getPolicyno() {
		return policyno;
	}

	public void setPolicyno(long policyno) {
		this.policyno = policyno;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public String getPolicymode() {
		return policymode;
	}

	public void setPolicymode(String policymode) {
		this.policymode = policymode;
	}
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "Policy [policyno=" + policyno + ", premium=" + premium + ", startdate=" + startdate + ", policymode="
				+ policymode + "]";
	}
	
}
