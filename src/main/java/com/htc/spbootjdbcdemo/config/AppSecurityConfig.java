package com.htc.spbootjdbcdemo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity  
public class AppSecurityConfig extends WebSecurityConfigurerAdapter
{
	@Autowired
	PasswordEncoder passwordEncoder;
    
	/*@Value("${spring.queries.users-query}")
    private String usersQuery;

    @Value("${spring.queries.roles-query}")
    private String rolesQuery;*/

	@Bean
	public PasswordEncoder getPasswordEncoder() {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder;
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
    	.passwordEncoder(passwordEncoder)
    	.withUser("htcuser").password(passwordEncoder.encode("123Welcome")).roles("USER")
    	.and()
    	.withUser("admin").password(passwordEncoder.encode("admin")).roles("USER", "ADMIN","EDITOR")
    	.and()
    	.withUser("newuser").password(passwordEncoder.encode("welcome")).roles("USER","EDITOR");
	}
	
	/*@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.jdbcAuthentication()
					.passwordEncoder(passwordEncoder)
					.usersByUsernameQuery(usersQuery)
					.authoritiesByUsernameQuery(rolesQuery);
	}*/

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
  		.antMatchers("/login").permitAll()
  		.antMatchers("/").hasRole("USER")
  		.antMatchers("/customerForm").hasAnyRole("USER")
  		.antMatchers("/addCustomer").hasAnyRole("USER")
  		.antMatchers("/addCustomerSucess").hasAnyRole("USER")
  		.antMatchers("/issuePolicyForm").hasAnyRole("EDITOR", "MANAGER")
  		.antMatchers("/issuePolicy").hasRole("EDITOR")
  		.antMatchers("/searchPolicyForm").hasRole("ADMIN")
  		.anyRequest()
  		.authenticated()
  		//.and().httpBasic();   //Default login screen
  		.and().formLogin()  //custom login screen
  			.loginPage("/login")
  			.failureUrl("/login?error=true") 
  			.usernameParameter("username")
  			.passwordParameter("password")
  			.defaultSuccessUrl("/")
  		.and()
  		.logout()
  			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
  			.logoutSuccessUrl("/login?logout=true")
  		.and()
  		.exceptionHandling()
  			.accessDeniedPage("/Access_Denied");
	}

	
}
