<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Form</title>
</head>
<body>
<h2> Customer Entry Form</h2>
 <font color="red">${errorMessage}</font>
<form action="addCustomer" method="post">
	<table>
		<tr><td>Customer Code</td> <td><input type="text" name="customerCode"/></td></tr>
	<tr><td>Customer Name</td> <td><input type="text" name="customerName"/></td></tr>
	<tr><td>Address</td> <td><input type="text" name="address"/></td></tr>
	<tr><td>Contact No</td> <td><input type="text" name="contactno"/></td></tr>
	<tr><td><input type="submit" value="Save Customer"/></td></tr>
	<td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</table>
	
</form>
</body>
</html>