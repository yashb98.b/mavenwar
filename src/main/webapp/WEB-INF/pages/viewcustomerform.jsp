<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<title>Customer Form</title>
	<link type="text/css" href="static/css/bootstrap.min.css" rel="stylesheet"/>
	<script  src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="   crossorigin="anonymous"></script>
	<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="static/js/customer.js"></script>
</head>
<body>
<%@include file="header.jsp"%>
<div class="container">
  <form class="form-horizontal" method="post" name="updateCustomerForm">
    <div class="row">
       <div class="col-sm-12" style="background-color: lavender;"><h2>View Customer Form</h2></div>
   </div>
   <div class="row">
      <div class="col-sm-12" style="background-color:lavender;">
  	<div class="form-group">
	      <label class="control-label col-sm-4" for="viewCustomerId">CustomerId</label>
        <div class="col-sm-8">
        	<input type="text"  id="viewCustomerId" name="viewCustomerId">
        	<input type="button" id="viewCustomer" class="btn btn-default" value="View Customer"/>
        </div> 
      	<span id="statusMessage"> </span>
      </div>
      </div>
   </div>
   <div class="row">
     <div class="col-sm-6" style="background-color: lavenderblush;">
     <br/><br/>
      <div class="form-group">
       	<label class="control-label col-sm-3" for="customerId">CustomerId</label>
        <div class="col-sm-5">
	        <input type="text" class="form-control" id="customerId" name="customerId">
	        <span id="customeridmsg"></span>
        </div>
      </div>
      <div class="form-group">
		<label class="control-label col-sm-3" for="customerName">Name</label>
		<div class="col-sm-5">
			<input type="text" class="form-control" id="customerName"
				name="customerName">
				<span id="customernamemsg"></span>
		</div>
      </div>
</body>
</html>