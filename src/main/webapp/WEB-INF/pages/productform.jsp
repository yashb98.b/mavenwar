<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Form</title>
</head>
<body>
<h2> Product Entry Form</h2>
 
<form action="addProduct" method="post">
	<table>
		<tr><td>Product Code</td> <td><input type="text" name="productCode"/></td></tr>
	<tr><td>Product Name</td> <td><input type="text" name="productName"/></td></tr>
	<tr><td>Category</td> <td><input type="text" name="category"/></td></tr>
	<tr><td>Price</td> <td><input type="text" name="price"/></td></tr>
	<tr><td>Quantity</td> <td><input type="text" name="quantity"/></td></tr>
	<tr><td><input type="submit" value="Save Product"/></td></tr>
	<td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</table>
</form>
</body>
</html>