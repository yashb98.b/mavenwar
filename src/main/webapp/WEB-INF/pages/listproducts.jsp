<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>


<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <h2>Product Details - Search Results</h2>
<table>
        <thead>
			<tr>
				<th>Product Code</th>
				<th>Product Name</th>
				<th>Category</th>
				<th>Price</th>
				<th>Quantity</th>
			</tr>
		</thead>
	<tbody>
	<c:forEach var="productList" items="${product}">
	<tr>
		<td> ${productList.productCode} </td>
		<td>${productList.productName}</td>
		<td> ${productList.category}</td>
		<td>${productList.price}</td>
		<td>${productList.productCode}</td>
	</tr>
	</c:forEach>
	</tbody>
</table>	
	
</body>
</body>
</html>