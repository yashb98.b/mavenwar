<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search Customer Details</title>
</head>
<body>
<h2>Product Details</h2>
	<table>
	<tr><td>Product Code</td> <td> ${product.productCode }</td></tr>
	<tr><td>Product Name</td> <td>${product.productName}</td></tr>
	<tr><td>Category</td> <td> ${product.category}</td></tr>
	<tr><td>Price</td> <td>${product.price}</td></tr>
	<tr><td>Quantity</td> <td>${product.quantity}</td></tr>
	</table>
</body>
</body>
</html>