<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link type="text/css" href="static/css/bootstrap.min.css" rel="stylesheet"/>
</head>
<body>
<div class="container">
	<h2>Login Screen</h2> 
	<h3>${errorMessage}</h3>
	<form  name="login" action="login" method="post">    
		<table class="table table-bordered table-striped">        
			<tr>
				<td>Username </td>
				<td><input type="text" name=username id="username"/></td>
			</tr>
			<tr>
				<td> Password </td>
				<td><input type="password" name="password" id="password"/></td>
			</tr>
			<tr>
				<td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<input type="submit" value="Sign In" /></td>
			</tr>

		</table>
	</form>
</div>
</body>
</html>