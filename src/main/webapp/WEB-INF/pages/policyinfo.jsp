<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>


<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <h2>Customer Policy Details - Search Results</h2>
<!--	<table>
	<tr><td>Policy No</td> <td> ${Policy.policyno }</td></tr>
	<tr><td>Premium</td> <td>${Policy.premium }</td></tr>
	<tr><td>Policy Mode</td> <td> ${Policy.policymode }</td></tr>
	<tr><td>Start Date</td> <td>${Policy.startdate }</td></tr>
	</table>-->
<table>
        <thead>
			<tr>
				<th>Policy No</th>
				<th>Premium</th>
				<th>Policy Mode</th>
				<th>Start Date</th>
			</tr>
		</thead>
	<tbody>
	<c:forEach var="policyTO" items="${policies}">
	<tr>
		<td> ${policyTO.policyno} </td>
		<td>${policyTO.premium }</td>
		<td> ${policyTO.policymode }</td>
		<td>${policyTO.startdate }</td>
	</tr>
	</c:forEach>
	</tbody>
	</table>	
	
</body>
</body>
</html>